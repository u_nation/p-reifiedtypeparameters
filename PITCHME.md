### <font color="DodgerBlue">Reified type parameters</font>

##### shibuya.apk #17

---

### <font color="DodgerBlue">自己紹介</font>

<img src=https://bytebucket.org/u_nation/p-reifiedtypeparameters/raw/27a6cef94a3115c7c5065807152196e8562da489/assets/twitter_icon.jpg width=100px height=100px>
<img src=https://bytebucket.org/u_nation/p-reifiedtypeparameters/raw/85b924af1ffe227dff7df8949ffa1a2f49acb45d/assets/logomark.png width=270px height=100px>

###### 遠藤　拓也
* Twitter [@u_nation](https://twitter.com/u_nation)
* Github [u-naiton](https://github.com/u-nation)

---

### <font color="DodgerBlue">目次</font>

1. Reified type parametersとは
2. 型消去
3. Reified type parameters使用前
4. Reified type parameters使用後
5. inline展開
6. Androidでの活用例
7. まとめ

---

### <font color="DodgerBlue">Reified type parametersとは</font>
- 具象化された型パラメータ
- [Kotlin M10で発表された](https://blog.jetbrains.com/kotlin/2014/12/m10-is-out/)
- [currently limited to inline functions only](https://github.com/JetBrains/kotlin/blob/master/spec-docs/reified-type-parameters.md#reified-type-parameters)
- ジェネリックメソッドで宣言した型パラメーターを実行時に具体的な型として扱うことができる

---

### <font color="DodgerBlue">型消去</font>

**ジェネリックスはJava1.4以前と互換性を提供するためコンパイルをすると型消去がおきる**

```kotlin
class GenericSample<T>(var value: T)
	
/*Bytecode→Decompile*/
@Metadata(/*略*/)
public final class GenericSample {
   private Object value;
   public final Object getValue() {return this.value;}
   public final void setValue(Object var1) {this.value = var1;}
   public GenericSample(Object value) {this.value = value;}
}
```
@[1](型パラメーターを宣言に持つジェネリッククラス)
@[3-10](Decompile結果仮型TはObjectに変換される)

---

### <font color="DodgerBlue">Reified type parameters使用前</font>
putとgetのジェネリックメソッド

```kotlin
/*FragmentのサブタイプのClassオブジェクトをHashMapで管理*/
val fragmentMap = HashMap<String, Class<out Fragment>>()

fun <T : Fragment> put(key: String, clazz: Class<T>) {
    fragmentMap.put(key, clazz)
}

@Suppress("UNCHECKED_CAST")
fun <T : Fragment> get(key: String): T? =
              fragmentMap[key]?.let { it.newInstance() as T }

/*呼び出し側*/
put("hoge", HogeFragment::class.java)
```
@[8-10](unchecked castの警告がでる)
@[12-13](冗長な呼び出し java。。。)

---

### <font color="DodgerBlue">Reified type parameters使用後</font>
putとgetのジェネリックメソッド

```kotlin
/*FragmentのサブタイプのClassオブジェクトをHashMapで管理*/
val fragmentMap = HashMap<String, Class<out Fragment>>()

inline fun <reified T : Fragment> put(key: String) {
    fragmentMap.put(key, T::class.java)
}


inline fun <reified T : Fragment> get(key: String): T? = 
              fragmentMap[key]?.let { it.newInstance() as T }

/*呼び出し側*/
 put<HogeFragment>("hoge")
```
@[9-10](安全なキャスト)
@[12-13](javaを書かなくてよくなった！)

---

### <font color="DodgerBlue">inline展開</font>

```kotlin
inline fun <reified T : Fragment> put(key: String) {
	fragmentMap.put(key, T::class.java)
}
	
/*呼び出し側*/	
put<HogeFragment>("hoge")
	
/*Bytecode→Decompile*/
String key$iv = "hoge";
this_$iv.getFragmentMap().put(key$iv, HogeFragment.class);
```
@[1-3](このinline関数が)
@[5-6](呼び出されたところで)
@[8-10](こう展開されます)
@[2,10](T::class.javaがHogeFragment.classに)

---

### <font color="DodgerBlue">Androidの活用例</font>
**startActivity**

```kotlin
/*拡張関数*/
inline fun <reified T : Activity> Activity.startActivity() {
	startActivity(Intent(this, T::class.java))
}

/*呼び出し側*/	
startActivity(Intent(this, HogeActivity::class.java))
startActivity<HogeActivity>()
```
@[1-4](Activityの拡張関数)
@[6-7](Before)
@[6,8](After)

---

### <font color="DodgerBlue">Androidの活用例</font>
**ViewModel**

```kotlin
/*拡張関数*/
inline fun <reified T : ViewModel> ViewModelProvider.get(): T = 
											this.get(T::class.java)
											
/*呼び出し側*/	
ViewModelProviders.of(this).get(HogeViewModel::class.java)
ViewModelProviders.of(this).get<HogeViewModel>()
```
@[1-3](ViewModelProviderの拡張関数)
@[5-6](Before)
@[5,7](After)

---

### <font color="DodgerBlue">Androidの活用例</font>
[RealmExtensions.kt](https://github.com/zaki50/GoogleRepositoryChecker/blob/bdf3d4d2ffa1a7a085d1fa17887d0d189f107c25/app/src/main/java/org/zakky/googlerepositorychecker/realm/RealmExtensions.kt)

```kotlin
/*拡張関数*/
inline fun <reified T : RealmModel> Realm.where(): RealmQuery<T> =
										where(T::class.java)

/*呼び出し側*/	
realm.where(Hoge::class.java)
realm.where<Hoge>()
```
@[1-3](Realmの拡張関数)
@[5-6](Before)
@[5,7](After)

---

### <font color="DodgerBlue">Androidの活用例</font>
[KotlinでViewDataBindingをｼｭｯとinflateするやつ](http://sys1yagi.hatenablog.com/entry/2017/05/12/134940) from [visible true](http://sys1yagi.hatenablog.com/entry/2017/05/12/134940) by [@sys1yagi](https://twitter.com/sys1yagi)

- 自動生成されたXXXBindingクラスのinflateメソッドをリフレクション

```kotlin
/*拡張関数*/
inline fun <reified T : ViewDataBinding> ViewGroup.inflateBinding(): T =
        T::class.java
                .getDeclaredMethod(
                        "inflate",
                        LayoutInflater::class.java,
                        ViewGroup::class.java,
                        Boolean::class.javaPrimitiveType
                )
                .invoke(null, LayoutInflater.from(context), this, false) as T
```

---

### <font color="DodgerBlue">Androidの活用例</font>

[KotlinでViewDataBindingをｼｭｯとinflateするやつ](http://sys1yagi.hatenablog.com/entry/2017/05/12/134940) from [visible true](http://sys1yagi.hatenablog.com/entry/2017/05/12/134940) by [@sys1yagi](https://twitter.com/sys1yagi)

- proguardで死ぬ旨を伝えたら正しい回避法を教えて頂きました！

```kotlin
-keep class * extends android.databinding.ViewDataBinding {
    public static ** inflate(android.view.LayoutInflater, 
    android.view.ViewGroup, boolean);
}
```

---

### <font color="DodgerBlue">まとめ</font>
- ::class.javaという冗長な表現を簡潔にできる
- 複数の型を汎用化したいケースで有効

---

## <font color="DodgerBlue">ご清聴ありがとうございました</font>
